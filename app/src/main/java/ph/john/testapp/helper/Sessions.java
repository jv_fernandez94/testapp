package ph.john.testapp.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by John Vincent on 6/19/2018.
 */

public class Sessions {

    private SharedPreferences sharedPreferences;
    private static Sessions instance = null;
    private String isLogin = "IS_LOGGED_IN";

    public static Sessions getInstance(Context context) {
        if (instance == null)
            instance = new Sessions(context);
        return instance;
    }

    private Sessions(Context context) {
        String PACKAGE_NAME = context.getPackageName();
        String name = PACKAGE_NAME + "." + "pg_pref";
        sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public void isLoggedIn(boolean value) {
        sharedPreferences.edit().putBoolean(isLogin, value).apply();
    }

    public boolean checkIsLoggedIn (String key){
        return sharedPreferences.getBoolean(key, false);
    }

    public void clearSession(){
        sharedPreferences.edit().clear().apply();
    }
}
