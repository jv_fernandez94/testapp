package ph.john.testapp.helper;

import android.text.TextUtils;
import android.widget.EditText;

/**
 * Created by Mina on 6/19/2018.
 */

public class Commons {

    public static boolean checkCredentials(EditText etEmpName, EditText etPass){
        String empNum = etEmpName.getText().toString().trim();
        String empPass = etPass.getText().toString().trim();

        if(TextUtils.isEmpty(empNum)){
            etEmpName.setError("Your Employee number is required");
            etEmpName.requestFocus();
            return false;
        }else if(empNum!=null || !TextUtils.isEmpty(empNum)){
            if(TextUtils.isEmpty(empPass)){
                etPass.setError("Your password is required");
                etPass.requestFocus();
                return false;
            }

        }

        return true;
    }
}
