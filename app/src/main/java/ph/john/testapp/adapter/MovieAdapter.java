package ph.john.testapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ph.john.testapp.R;
import ph.john.testapp.model.Movie;


/**
 * Created by Mina on 6/25/2018.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    private List<Movie> movieList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date;
        public ImageView poster;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView)view.findViewById(R.id.tvTitle);
            date = (TextView)view.findViewById(R.id.tvDate);
            poster = (ImageView)view.findViewById(R.id.imgPoster);
        }
    }

    public MovieAdapter(List<Movie> movies){
        this.movieList = movies;
    }

    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieAdapter.MyViewHolder holder, int position) {
        final Movie movie = movieList.get(position);
        holder.title.setText(movie.getMovieTitle());
        holder.date.setText(movie.getMoviewDate());
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }


}
