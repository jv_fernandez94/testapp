package ph.john.testapp.model;

/**
 * Created by Mina on 6/25/2018.
 */

public class Movie {

    private String movieTitle;
    private String moviewDate;
    private String movieDirector;
    private String movieDescription;
    private String movieImgUrl;
    private int moviePrice;

    public Movie(){}

    public Movie(String movieTitle, String moviewDate, String movieDirector, String movieDescription, String movieImgUrl, int moviePrice) {
        this.movieTitle = movieTitle;
        this.moviewDate = moviewDate;
        this.movieDirector = movieDirector;
        this.movieDescription = movieDescription;
        this.movieImgUrl = movieImgUrl;
        this.moviePrice = moviePrice;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMoviewDate() {
        return moviewDate;
    }

    public void setMoviewDate(String moviewDate) {
        this.moviewDate = moviewDate;
    }

    public String getMovieDirector() {
        return movieDirector;
    }

    public void setMovieDirector(String movieDirector) {
        this.movieDirector = movieDirector;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getMovieImgUrl() {
        return movieImgUrl;
    }

    public void setMovieImgUrl(String movieImgUrl) {
        this.movieImgUrl = movieImgUrl;
    }

    public int getMoviePrice() {
        return moviePrice;
    }

    public void setMoviePrice(int moviePrice) {
        this.moviePrice = moviePrice;
    }


}
